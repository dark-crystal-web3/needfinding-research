## Goals for research sessions

- User research with participants themselves as users, but also relaying their sense about the users of their own project and the ecosystem in general.
- Feedback about any pitfalls, trade-offs or weaknesses about this approach that we may not be seeing.
- Discuss possibility to integrate our projects.

## Description of what we're building

A social recovery app with two major roles: secret owner and a recovery partner. The app does not require the recovery partner to remember anything except the access information for their main Ethereum wallet. The app should be secret-agnostic, meaning it can backup any kind of secret, including but not limited to an Ethereum private key. The app should allow for both anonymity of the secret owner AND of the recovery partners.

Our [WIP design document](https://gitlab.com/dark-crystal-web3/eth-social-recovery-mvp) has a more detailed description.

### Our users:

Secret owner is fairly technical, able to run a rust CLI app locally, has different kinds of wallets and familiarity with burners and mixers. 

Recovery partner has a main Ethereum wallet that they continue using and are unlikely to lose access to.

Both users are familiar with trustworthy encrypted messengers/email services.

Architecturally: There are three main code bases: a hosted dapp where recovery partners can connect with popular wallets; a smart contract; and a rust client for use by the secret-owner, which has a locally-served browser interface and can be run on an air-gapped machine

### Recovery partner set-up steps:

  - Connects Metamask to our dapp
  - Is prompted to sign a standard phrase: `signature(standard_phrase)` 
  - Our dapp uses this signature as the seed for a new public key: `public_key_seed(signature(standard_phrase))`
  - Recovery partner sends this public key to the secret owner

### Secret owner set-up steps:

  - Collects all the public keys from their recovery partners
  - Generates the shares using the local rust app and encrypts them to each pub key
  - Local rust app generates some sort of payment request containing the encrypted shares packaged for our smart contract
  - Secret owner uses burner key and mobile wallet to send transaction

### Recovery steps:

  - Recovery partner gets either 1) the secret owner's address OR 2) a lookup phrase
  - Recovery partner connects to the dapp and regenerates their signature
  - Recover partner uses their public key and either 1) the secret owner's address OR 2) the lookup phrase to recover their encrypted share
  - Recovery partner decrypts their share
  - Recovery partner transmits their decrypted share to secret owner
  - Secret owner uses the local rust app to recombine their recovery partners' shares to recover their secret
 
## Questions for participants:

1. How likely would you be to use a system like this?
2. What would be your biggest concern about using a system like this?
3. Are there any additional features or security guarantees not yet discussed that would be important to you?
4. Would cost be a concern when using an app like this? If so, how much would you be willing to pay in transaction fees?
5. What tools and apps would you want to use to interact i.e. do you have a mobile wallet that can scan a payment request? Are there any wallets our dapp component *must* support in your opinion?
6. How many recovery partners would you choose, and with what threshold? (minimum number for recovery)

#### Feature rankings:

How would you rate the importance of these features? (From 1: unimportant to 5: essential)

1. Recovery partners stay anonymous
2. Secret owner stays anonymous (we think this would require them sending transactions from a burner wallet, which had been funded with eth that had been through a mixer)
3. Secret that is backed up is able to be anything (eg: PGP private key OR Ethereum private key OR arbitrary message)
4. Secret owner can generate and encrypt shares on an air-gapped computer (secret can stay in cold storage)
5. Secret owner can back up more than one secret with our tools
6. Secret owner can back up more than one secret with our tools AND use the same set of recovery partners

#### Scenarios:

How likely do you think these recovery scenarios are? (From 1: impossible to 5: certain)

1. Secret owner can remember the address the transactions to our smart contract were published with (even if it was a burner account)
2. Secret owner has forgotten their main Ethereum address
3. Secret owner doesn't have a main Ethereum address
4. Secret owner used an arbitrary string as their "lookup key" and has forgotten it
5. Secret owner loses their computer AND forgets their main Ethereum address AND the address the transactions were sent to the smart contract with (burner) but remembers who the recovery partners are
6. Secret owner forgets who the recovery partners are
7. Secret owner dies, recovery partners know who each other are
8. Secret owner dies, recovery partners do not know who each other are
9. Recovery partner loses access to their main Ethereum account
10. Recovery partner uses an insecure service to send the recovered share back to the secret owner

